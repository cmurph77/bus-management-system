/**
 * @author: cianmurphy
 */
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class StopDatabase {

    private static File stopFile = new File("stops.txt");
    private static File stopTimesFile = new File("stop_times.txt");


    private static TST<Stop> Name_Tree = new TST();
    private static BST<Integer,Stop> StopID_Tree = new BST();


    StopDatabase() throws IOException {
        createTreeDataStructures();  // call the set up methon
    }


    /**
     * @brief This method creates the BST for storing Stop IDs and the TST for storing stop Names.
     */
    public static void createTreeDataStructures() throws IOException {
        List<String> list = Files.readAllLines(stopFile.toPath(), Charset.defaultCharset());   // read in the file to a list
        for(int i = 1; i < list.size(); i++){
            String[] line = list.get(i).split(",");   // split the each line up into an array
            Stop newStop = new Stop(line);     // create a stop with an array of its information
            if(newStop.stop_id > 0) {
                int stop_id = newStop.stop_id;
                StopID_Tree.put(stop_id,newStop); // put the stop in the BST using the stop_id as the key
            }
            if(newStop.stop_name.length() >= 1){
                Name_Tree.put(newStop.stop_name,newStop);   // put the new stop in the TST with the its name as the key
            }
        }
    }


    /**
     * @brief This method finds a stop with its full name passed in
     *
     * @param stopName the FULL name of the stop you want to find
     * @return the stop that was found (returns null if no stop was found)
     */
    public static Stop getStopFullName(String stopName){
        Stop result = Name_Tree.get(stopName);
        return result;
    }

    /**
     * @brief This method gets all the stops that start with the partial name passed in
     *
     * @param partialName The partial name of the stops you want to find
     * @return an array list of stops that start with the partial name passed in
     */
    public static ArrayList<Stop> getStopsPartialName(String partialName){
        ArrayList<Stop> stopList = new ArrayList();   // create a new array list
        Iterable<String> result = Name_Tree.keysWithPrefix(partialName);   // an iterable of all the stop names that begin with partialName
        for(String name : result){
            Stop stop = getStopFullName(name); // get the stop from the full name
            stopList.add(stop);   // add the stop to the aray of stops
        }
        return stopList;
    }

    /**
     * @brief This method gets a trip that matches a time.
     * @param searched_time the time of the trips you would like
     */
    public static void getTripByTime(String searched_time) throws IOException{
        int count = 0;
        String[] timeSplit = searched_time.split(":");  // split the searched time into hours, mins and secs
        int searched_hours = Integer.parseInt(timeSplit[0]);
        int searched_mins = Integer.parseInt(timeSplit[1]);
        int searched_secs = Integer.parseInt(timeSplit[2]);
        List<String> list = Files.readAllLines(stopTimesFile.toPath(), Charset.defaultCharset());   // read in the file to a list
        for(int i = 1; i < list.size();i++){  // start at 1 since the first line in the list are the keys
            String[] line = list.get(i).split(",");
            String hours = line[1].trim();                              // trims the white space before the hours integer
            String[] time = hours.split(":");                     // the arrival time is the second entry in the array
            int curr_hours = Integer.parseInt(time[0]);
            int curr_mins = Integer.parseInt(time[1]);
            int curr_secs = Integer.parseInt(time[2]);
            if(curr_hours == searched_hours && curr_mins == searched_mins && curr_secs == searched_secs){
                    printTripInfo(line); // print out the trip info if the times match

            }
        }
    }

    /**
     * @brief Finds a stop from its ID
     *
     * @param stop_id The ID of the stop you want to find
     * @return The stop that was found or null if no stop was found
     */
    public static Stop getStopFromID(int stop_id){
        Stop stop = StopID_Tree.get(stop_id);
        //Stop.printStopInformation(stop);
        return stop;
    }

    /**
     * @brief This method returns the name of the stop that matches the ID entered
     * @param stop_id The ID of the stop you want
     * @return The name of the stop or an empty string if the stop was not found
     */
    public static String getNameFromID(int stop_id){
        Stop stop = StopID_Tree.get(stop_id);
        if(stop == null) return "";
        return stop.stop_name;
    }

    /**
     * @brief This method prints out trip info.
     * @param tripInfo an array of the trip details of the trip to be printed out.
     */
    public static void printTripInfo(String[] tripInfo){
        System.out.println("\nTrip ID: " + tripInfo[0]);
        System.out.println("Arrival Time: " + tripInfo[1]);
        System.out.println("Departure Time: " + tripInfo[2]);
        System.out.println("Stop ID: " + tripInfo[3]);
        System.out.println("Stop Sequence: " + tripInfo[4]);
        System.out.println("Stop Headsign: " + tripInfo[5]);
        System.out.println("Pickup Type: " + tripInfo[6]);
        System.out.println("Drop-Off Type: " + tripInfo[7]);
        System.out.println("Shape Distance Travelled: " +  tripInfo[8] + "km");



    }

    /**
     * @brief This method prints out the information for a list of stops
     * @param list the list of the stops that you want printed out
     */
    public static void printMultiStopInfo(ArrayList<Stop> list){
        for(int i = 0; i < list.size();i++){
            Stop.printStopInformation(list.get(i));  // print out each stop in the list
        }
    }

}
