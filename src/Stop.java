/**
 * @author cianmurphy 
 */

public class Stop {

    int stop_id = 0;
    int stop_code = 0;
    String stop_name = "";
    String stop_desc = "";
    double stop_lat = 0;
    double stop_long = 0;
    String zone_id = "";
    String stop_url = "";
    String location_type = "";
    int parent_station = 0;

    /**
     * @brief constructor for the stop data type
     * @param line a string arrayw with all the data that is needed to create a stop
     *
     */
    Stop(String[] line) {

        try {
            this.stop_id = Integer.parseInt(line[0]);
            this.stop_code = Integer.parseInt(line[1]);
            this.stop_name = reOrderName(line[2]);
            this.stop_desc = line[3];
            this.stop_lat = Double.parseDouble(line[4]);
            this.stop_long = Double.parseDouble(line[5]);
            this.zone_id = line[6];
            this.stop_url = line[7];
            this.location_type = line[8];

            if (line.length == 10) {
                this.parent_station = Integer.parseInt(line[9]);
            }
        } catch (NumberFormatException e) {
            //System.out.println("Incomplete Stop Info, Stop will not add to Database.: ");
        }
    }


    /**
     * @brief This method prints out all the stop information to the user
     * @param stop the Stop that you want printed out
     */
    public static void printStopInformation(Stop stop){
        System.out.println("Stop ID:  " + stop.stop_id);
        System.out.println("Stop Code: " + stop.stop_code);
        System.out.println("Stop Name: " + stop.stop_name);
        System.out.println("Stop Desc: " + stop.stop_desc);
        System.out.println("Stop Latitude: " + stop.stop_lat);
        System.out.println("Stop Longitude: " + stop.stop_long);
        System.out.println("Zone Id: " + stop.zone_id);
        System.out.println("Stop URL: " + stop.stop_url);
        System.out.println("Location Type: " + stop.location_type);
        if(stop.parent_station != 0) System.out.println("Parent Station: " + stop.parent_station);
        System.out.println("");

    }

    /**
     * @brief This function re arranges a String by moving the first word to the end
     *
     * @param name This is the name String to be reordered
     * @return the re ordered name
     */
    public String reOrderName(String name){
        String[] array = name.split(" ");  // split up the string into an array of words
        int size = array.length;
        StringBuilder newName = new StringBuilder();
        String firstWord = array[0];
        for(int i = 1; i < size;i++){  // start at 1 since we want to move the first word to the back
            newName.append(array[i]);  // reshuffle the words up in the string
            newName.append(" "); // add in a space between words
        }
        newName.append(firstWord);  // append the first word to the end
        return newName.toString();
    }

}






















