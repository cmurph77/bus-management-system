/**
 * @author: cianmurphy
 */
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;

public class BusNetworkTests {

    @Test
    public void testConstructor() throws IOException {
        BusNetwork net = new BusNetwork();
    }

    @Test
    public void testMaxStopID() throws IOException {
        int reuslt = BusNetwork.maxStopID();
        System.out.println(reuslt);
    }

    @Test
    public void testFindShortestPathCost() throws IOException{
        BusNetwork net = new BusNetwork();
        int start = 209; int end = 449;
        double cost= net.findShortestPathCost(1477,1394);
        System.out.println("The cost of the shortest path is " + cost);

    }

    @Test
    public void testFindShortestPath() throws IOException {
        BusNetwork net = new BusNetwork();
        int start = 1477;
        int end = 1394;
        ArrayList<Integer> list = net.findShortestPath(start, end);
        BusManagementSystem.printBusRoute(list);

    }

}
