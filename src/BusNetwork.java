import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
/**
 * @author: cianmurphy
 */
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BusNetwork {
    static EdgeWeightedDigraph map;

    BusNetwork() throws IOException{
        createMap();
    }


    /**
     * @brief The method finds the cost of the shortest path between two vertices.
     *
     * @param start The stop id for the starting stop
     * @param end the stop id for the end stop
     * @return the assoosiated cost for this path
     */
    public static double findShortestPathCost(int start,int end){
        DijkstraSP d = new DijkstraSP(map,start);     // create a new dijstra object.
        return d.distTo(end);                         // return the shortest path between the two vertices.
    }

    /**
     * @brief This method finds the shortest path between two vertices
     *
     * @param start The stop id for the starting stop
     * @param end the stop id for the end stop
     * @return An array of stop numbers that are on the shortest path.
     */
    public static ArrayList<Integer> findShortestPath(int start, int end){
        int end_stop = 0;                                       // declaring variable for use later outside the loop
        DijkstraSP d = new DijkstraSP(map,start);               // create a new dijkstra object
        ArrayList<Integer> listOfStop_IDs = new ArrayList<>();
        Iterable<DirectedEdge> path = d.pathTo(end);           // get the list of stops on the path
        for(DirectedEdge e: path) {
            int stop_id = e.from();    // get the stop id
             end_stop = e.to(); // get the stop at the end. only used after the loop
            listOfStop_IDs.add(stop_id);  // add the stop id to the list
        }
        listOfStop_IDs.add(end_stop);

        return listOfStop_IDs;
    }

    /**
     * @brief This method creates the graph data structure that is used to store the trasfer data.
     */
    public void createMap() throws IOException {
        map = new EdgeWeightedDigraph(maxStopID() + 1);  // create an empty graph data structure
        addFromTransfers();  // add in edges from transfers.txt document
        addFromStopTimes();  // add in edges from stop_times.txt document
    }

    /**
     * @brief This method addes edges to the graph from the stop_times.txt file
     */
    public void addFromStopTimes() throws IOException {
        double cost = -1;    // initalize variables
        int prev_tripID = 0;
        int prev_stopID = 0;
        List<String> list = Files.readAllLines(new File("stop_times.txt").toPath(), Charset.defaultCharset());
        for(int i = 1; i < list.size(); i++){
            String[] line = list.get(i).split(",");                     // split up the values on the line
            int current_tripID = Integer.parseInt(line[0]);
            int from_stop_id = Integer.parseInt(line[3]);
            if(current_tripID == prev_tripID){                // only add the edge if the trip_ids are the same
                DirectedEdge e = new DirectedEdge(from_stop_id,prev_stopID,1);  // add a new a edge to the graph
                map.addEdge(e);
            }
            prev_stopID = from_stop_id; // set for the net itereation
            prev_tripID = current_tripID;
        }
    }

    /**
     * @brief this method adds edges for the transfers.txt file
     */
    public void addFromTransfers() throws IOException {
        double cost = -1; // initalize variable
        List<String> list = Files.readAllLines(new File("transfers.txt").toPath(), Charset.defaultCharset());  // read in the values
        for(int i = 1; i < list.size(); i++){
            String[] line = list.get(i).split(",");
            int from_stop_id = Integer.parseInt(line[0]);   // the source of the edge
            int to_stop_id = Integer.parseInt(line[1]);     // the next entry is the destination of the edge
            int transfer_type = Integer.parseInt(line[2]);  // read in the trasfer type
            if(transfer_type == 0){
                cost = 2;
            } else if(transfer_type == 2){
                double min_transfer_time = Double.parseDouble(line[3]);
                cost = min_transfer_time/100;
            }
            DirectedEdge e = new DirectedEdge(from_stop_id,to_stop_id,cost);  // add a new directed edge
            map.addEdge(e);
        }
    }

    /**
     * @brief this method finds the maxium stop id in the list of stops from stops.txt
     *
     * @return the max stop id
     */
    public static int maxStopID() throws IOException {
        int max = 0;
        List<String> list = Files.readAllLines(new File("stops.txt").toPath(), Charset.defaultCharset());   // read in the file to a list
        for(int i = 1; i < list.size(); i++) {
            String[] line = list.get(i).split(",");   // split up each line up into an array
            int current_ID = Integer.parseInt(line[0]);
            if (current_ID > max) max = current_ID; // find the max

        }
        return max;
    }
}
