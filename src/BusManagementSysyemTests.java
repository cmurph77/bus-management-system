/**
 * @author: cianmurphy
 */
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BusManagementSysyemTests {

    @Test
    public void testSearchTripTimes() throws IOException {
        BusManagementSystem b = new BusManagementSystem();
        b.searchTripTimes();
    }

    @Test
    public void testCheckValidTime() throws IOException {
        Boolean result = BusManagementSystem.checkValidTime("23:45:55");
        assertTrue(result);

        result = BusManagementSystem.checkValidTime("23:35");
        assertFalse(result);

        result = BusManagementSystem.checkValidTime("24:35:45");
        assertFalse(result);

        result = BusManagementSystem.checkValidTime("14:80:45");
        assertFalse(result);

        result = BusManagementSystem.checkValidTime("14:35:67");
        assertFalse(result);

        result = BusManagementSystem.checkValidTime("2tser:35:45");
        assertFalse(result);


    }


}
