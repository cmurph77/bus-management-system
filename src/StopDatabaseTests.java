/**
 * @author: cianmurphy
 */
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;

public class StopDatabaseTests {

    @Test
    public void testConstructor() throws IOException {
        StopDatabase s = new StopDatabase();
    }


    @Test
    public void testGetStopFullName() throws IOException {
        StopDatabase s = new StopDatabase();
        Stop found = s.getStopFullName("UNGLESS WAY FS GUILDFORD WAY WB");
        Stop.printStopInformation(found);

    }

    @Test
    public void testGetStopPartialName() throws IOException{
        StopDatabase s = new StopDatabase();
        ArrayList<Stop> matchingStops = s.getStopsPartialName("UNGLESS");
        System.out.println();
    }

    @Test
    public void testGetTripByTime() throws IOException {
        StopDatabase.getTripByTime("5:27:33");
    }



}