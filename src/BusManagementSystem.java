/**
 * @author cianmurphy
 */
import java.awt.desktop.SystemEventListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class BusManagementSystem {

     public static StopDatabase stopDatabase;
     public static BusNetwork busNetwork;
    private static boolean running = true;

    public static void main(String[] args) throws IOException {
        System.out.println("Starting the program");
        init_system();
        home();

    }

    /**
     * @brief This method runs the home screen interface for the UI.
     * @throws IOException
     */
    public static void home() throws IOException {
        Scanner s = new Scanner(System.in);
        System.out.println("\n\n----------------------------------- Home------------------------------------------------");

        while(true) {

            System.out.println("\n Please read the options below and enter the number you want");
            System.out.println("    1 - Search For Stop by Name");
            System.out.println("    2 - Find a Bus Route");
            System.out.println("    3 - Find Trip Times");
            System.out.println("    4 - Search for a Stop by ID");

            String input = s.next();
            int selectedInput = Integer.parseInt(input);
            switch(selectedInput){
                case 1:
                    searchStops();
                    break;

                case 2:
                    searchShortestPath();
                    break;

                case 3:
                    searchTripTimes();
                    break;

                case 4:
                    searchStopByID();
                    break;
            }

        }


    }

    /**
     * @brief This is the method runs the UI for searching for the shortest path.
     */
    public static void searchShortestPath() throws IOException {
        System.out.println("\n\n-------------------- Welcome to the Bus Route Finder -----------------------------------");
        while(true){
            Scanner s = new Scanner(System.in);
            System.out.println("\n Please enter the Stop ID of the stop that you are at or \"-1\" to go to the homepage.");
            int from = s.nextInt();
            if(from == -1) home();
            System.out.println("Now please enter the Stop ID for the stop you would like to go to");
            int to = s.nextInt();
            double cost = busNetwork.findShortestPathCost(to,from);
            if(cost != Double.POSITIVE_INFINITY) {
                System.out.println("This journey will cost you: " + cost);
                ArrayList<Integer> route = busNetwork.findShortestPath(to, from);
                printBusRoute(route);
                System.out.println("\n If you would like to see the stops by their name enter \"y\"");
                String answer = s.next();
                if(answer.equals("y")) printBusRouteNames(route);
            }else{
                System.out.println("There exists no path between the chosed stops, please try again.");
            }

        }
    }

    /**
     * @brief This method runs the UI for the searching for a stop with the name
     * @throws IOException
     */
    public static void searchStops() throws IOException {
        System.out.println("\n\n----------------------- Welcome to the Stop Finder -------------------------------------");
        while(true){
            System.out.println("Please enter a the name of the Stop you are looking for in CAPITALS, or \"back\" to return to home menu");
            Scanner s = new Scanner(System.in);
            String nameEntered = s.next();
            if(nameEntered.equals("back")) home();
            System.out.println("Searching for stops with the name name " + nameEntered + "...");
            ArrayList<Stop> list = StopDatabase.getStopsPartialName(nameEntered);
            if (list.size() >= 1) System.out.println("Found " + list.size() + " Stops matching the name you entered.");
            if (list.size() == 0) System.out.println("No Stops were found.");

            StopDatabase.printMultiStopInfo(list);
        }

    }

    /**
     * @brief This method runs the UI for searching for a trip by time
     * @throws IOException
     */
    public static void searchTripTimes() throws IOException {
        System.out.println("\n\n----------------------- Welcome to the Trip Finder -------------------------------------");

        while(true){
            System.out.println("Please enter the arrival time you would like to search for in the format hh:mm:ss or enter \"-1\" to go back to home");
            Scanner s = new Scanner(System.in);
            String timeEntered = s.next();
            if(timeEntered.equals("-1")) home();
            if(checkValidTime(timeEntered)) {
                StopDatabase.getTripByTime(timeEntered);
            }
            System.out.println("\n");
        }
    }

    /**
     * @brief This method runs the UI for serching for a stop with the Stop ID
     * @throws IOException
     */
    public static void searchStopByID() throws IOException {
        System.out.println("\n\n---------------------- Welcome to the Stop Finder -------------------------------------");

        while(true){
            System.out.println("\nPlease enter the Stop ID you wish to find or \"-1\" to go home");
            Scanner s = new Scanner(System.in);
            int id_entered = s.nextInt();
            if (id_entered == -1) home();
            Stop stop = stopDatabase.getStopFromID(id_entered);
            if(stop == null) {
                System.out.println(id_entered + " is not a Valid Stop Number, Please Try Again");
            }else Stop.printStopInformation(stop);
        }
    }

    /**@brief Thie method prints out the bus route to the user as Stop IDS
     * @param route : This is an array list of stop ids that are to be printed out to the user.
     */
    public static void printBusRoute(ArrayList<Integer> route){
        System.out.print("Your route is: ");
        for(int i = 0 ; i < route.size();i++){
            System.out.print(route.get(i) + " -> ");
        }
    }

    /**
     * @breif This method prints out the stop names on the bus route to the user
     */
    public static void printBusRouteNames(ArrayList<Integer> route){
        System.out.println("The journey starts at " + stopDatabase.getNameFromID(route.get(0)));
        System.out.println("These are the stops you visit on the way.");
        for(int i = 1; i < route.size();i++){
            int stop_id = route.get(i);
            String stop_name = stopDatabase.getNameFromID(stop_id);
            System.out.println("    " + stop_name);
        }
    }

    /**
     * @brief A method to check whether a string is a valid time
     *
     * @param time : The string to be checked if it is in valid time.
     * @return a boolean of whether the string is a valid time
     */
    public static Boolean checkValidTime(String time){
        int hours = 0; int mins = 0; int secs = 0;
        String[] timeSplit = time.split(":");  // split the searched time into hours, mins and secs
        if(timeSplit.length != 3) {
            System.out.println("Please Enter the time in the correct Format, hh:mm:ss");
            return false;
        }

        try {
             hours = Integer.parseInt(timeSplit[0].trim());
             mins = Integer.parseInt(timeSplit[1].trim());
             secs = Integer.parseInt(timeSplit[2].trim());
        }catch(NumberFormatException e){
            System.out.println("Please Enter only Integer Numbers");
            return false;
        }
        if(hours > 23) {
            System.out.println("Hours is out of bounds, Please try again");
            return false;
        }
        if(mins > 59){
            System.out.println("Minutes is out of bounds, Please try again");
            return false;
        }
        if(secs > 59){
            System.out.println("Seconds is out of bounds, Please try again");
            return false;
        }


        // time is valid if it passes all the tests
        return true;
    }

    /**
     * @brief This method initiallizes all the data structures that are used by the program.
     *
     * @throws IOException
     */
    public static void init_system() throws IOException {
        System.out.println("Loading Stop Data in....");
        stopDatabase = new StopDatabase();
        System.out.println("Stop Data Loading Complete.");
        System.out.println("Loading Transfer Data in....");
        busNetwork = new BusNetwork();
        System.out.println("Trasfer Data Loading Complete.");
        System.out.println("Set Up Complete.");

    }


}
