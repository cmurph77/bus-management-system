

import java.util.Arrays;

public class GraphMatrix {
    double[][] adj_matrix;
    int intersections;

    GraphMatrix(int vertices){
        this.intersections = vertices;
        adj_matrix = new double[intersections][intersections];
    }

    public void addEdge(int source, int destination, double distance) {
        adj_matrix[source][destination] = distance;
    }

    public void printGraph(){
        for(int i = 0;i < intersections;i++){
            System.out.println(Arrays.toString(adj_matrix[i]));
        }
    }
}
